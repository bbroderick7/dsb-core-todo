package com.celestial.dsb.core.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MetatagDAO
{
    Connection con = null;

    private static String tableName = "metatag";
    private static String joiningTable = "linkToMetatag";

    public void saveTag(String name) throws SQLException
    {
        PreparedStatement updateStmt = null;
        String sql = "insert into " + tableName + " values ( ? )";
        try
        {
            con = DBConnector.getConnector().getConnection();
            updateStmt = con.prepareStatement(sql);
            updateStmt.setString(1, name);
            updateStmt.executeUpdate();
        } catch (Exception e)
        {
            e.printStackTrace();
        } 
    }

    public ArrayList<String> getAllTags()
    {
        String sql = "select * from " + tableName;
        ResultSet result;
        ArrayList<String> metatags = new ArrayList<>();
        PreparedStatement selectStmt = null;
        try
        {
            con = DBConnector.getConnector().getConnection();
            selectStmt = con.prepareStatement(sql);
            result = selectStmt.executeQuery();
            while (result.next())
            {
                metatags.add(result.getString(1));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return metatags;
    }

    public ArrayList<String> getTagsForLink(int linkId)
    {
        String sql = "select * from " + tableName + " INNER JOIN " + joiningTable + " ON "
                + joiningTable + ".tag_name = " + tableName + ".tag_name where " + joiningTable + ".link_id = ? ";
        ResultSet result;
        ArrayList<String> tags = new ArrayList<>();
        PreparedStatement selectStmt = null;
        try
        {
            con = DBConnector.getConnector().getConnection();
            selectStmt = con.prepareStatement(sql);
            selectStmt.setInt(1, linkId);
            result = selectStmt.executeQuery();
            while (result.next())
            {
                tags.add(result.getString(1));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return tags;
    }
}
